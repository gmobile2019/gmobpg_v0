<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Api_model
 *
 * @author gmobile
 */
class Api_model extends CI_Model{
    
    
    private $pg;
    
    public function __construct()
    {
            parent::__construct();
            $this->pg=$this->load->database('pg',TRUE);//load shopping database configuration
    }
    
    function save_tola_transaction($data){
        
        return $this->pg->insert('tola_transactions',$data);
    }
    
    function tola_transactions($channel,$operatorTxnID,$status){
        
        if($channel <> NULL){
            
            $where .=" AND channel='$channel'";
        }
        
        if($operatorTxnID <> NULL){
            
            $where .=" AND operatorTxnID='$operatorTxnID'";
        }
        
        if($status <> NULL){
            
            $where .=" AND status='$status'";
        }
        
        return $this->pg->query("SELECT accountName,amount,amountType,"
                                . "channel,operatorTxnID,status,"
                                . "currency,customerReference,txnType,"
                                . "transactionDate,msisdn,operatorTxnID,"
                                . "serviceID,referenceNumber,tolaTxnReference "
                                . "FROM tola_transactions "
                                . "WHERE id is not null $where "
                                . "ORDER BY id DESC")->result();
    }
}
