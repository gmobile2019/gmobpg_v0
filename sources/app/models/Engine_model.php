<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Engine_model
 *
 * @author gmobile
 */
class Engine_model extends CI_Model{
    //put your code here
    
    private $pg;
    
    public function __construct()
    {
            parent::__construct();
            $this->pg=$this->load->database('pg',TRUE);//load shopping database configuration
    }
    
    function kilimo_one_transactions($instance,$status,$serviceid){
        
        if($instance <> NULL){
            
            $where .=" AND ind='$instance'";
        }
        
        if($status <> NULL){
            
            $where .=" AND status='$status'";
        }
        
        if($serviceid <> NULL){
            
            $where .=" AND serviceID='$serviceid'";
        }
        
        return $this->pg->query("SELECT id,amount,referenceNumber,"
                . "channel,msisdn,operatorTxnID,receivedTimestamp "
                . "FROM tola_transactions "
                . "WHERE id is not null $where "
                . "ORDER BY  receivedTimestamp DESC "
                . "LIMIT ".$this->config->item('db_data_select_limit'))->result();
    }
    
    function saveTransaction($data){
        
                $this->pg->trans_start();
                foreach($data as $sql){

                    $this->pg->query($sql);
                }
                $this->pg->trans_complete();
        return $this->pg->trans_status();
    }
}
