<?php
/*
 * Tola endpoint interface for kilimo one application
 */
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'/libraries/REST_Controller.php');
 
class Interface1 extends REST_Controller{
    
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Api_model');
    }

    /*
     * receive data
     */
    function receive_post(){
        $logf='./logs/receive_'.date('Y-m-d').'.log';
        
        $logfile=file_exists($logf);

        if($logfile){

            $filesize=filesize($logf);

            if($filesize > 1048576){
                rename($logf,"./logs/receive_".date('His')."_".date('Y-m-d').'.log');
            }
        }
        
        $reqid=uniqid();
        
        //receive request
        $request =$this->input->raw_input_stream;
        error_log("$request ".date('Y-m-d H:i:s')."[$reqid | kilimoOne | Tola] \n", 3, $logf);
        $data=json_decode($request);
        
        if(!$data){
            
            error_log("empty data [$reqid | kilimoOne | Tola]".date('Y-m-d H:i:s')."\n", 3, $logf);
            $this->response(array("success"=>false), 200);
            exit;
        }
        
        
        $acname=(string)$data->accountname;
        $amount=(float)$data->amount;
        $amounttype=(string)$data->amounttype;
        $channel=(string)$data->channel;
        $currency=(string)$data->currency;
        $customerreference=(string)$data->customerreference;
        $date=(string)$data->date;
        $reference=(string)$data->reference;
        $referencenumber=(string)$data->sourcereference;
        $operatorreference=(string)$data->operatorreference;
        $msisdn=(string)$data->msisdn;
        $target=(string)$data->target;
        $type=(string)$data->lodgement;
        
        //verify request
        $system_mac=strtoupper(md5($msisdn.':'.$date.':'.$this->config->item('tola_secret')));
        if((string)$data->mac <> $system_mac){
            
            error_log("incorrect mac $system_mac [$reqid | kilimoOne | Tola]".date('Y-m-d H:i:s')."\n", 3, $logf);
            $this->response(array("success"=>false), 200);
            exit;
        }
        
        //check transaction id and channel
        if($this->Api_model->tola_transactions($channel,$operatorreference) <> NULL){
            
            error_log("duplicate transaction [$reqid | kilimoOne | Tola]".date('Y-m-d H:i:s')."\n", 3, $logf);
            $this->response(array("success"=>true), 200);
            exit;
        }
        
        $txn=array(
            'accountName'=>$acname,
            'amount'=>$amount,
            'amountType'=>$amounttype,
            'channel'=>$channel,
            'currency'=>$currency,
            'customerReference'=>$customerreference,
            'transactionDate'=>$date,
            'msisdn'=>$msisdn,
            'operatorTxnID'=>$operatorreference,
            'serviceID'=>$target,
            'referenceNumber'=>$referencenumber,
            'tolaTxnReference'=>$reference,
            'txnType'=>$type,
            'receivedTimestamp'=>date('Y-m-d H:i:s'),
        );
        
        //save transaction
        $sve=$this->Api_model->save_tola_transaction($txn);
        
        if($sve){
            
            error_log("transaction saved [$reqid | kilimoOne | Tola]".date('Y-m-d H:i:s')."\n", 3, $logf);
            $this->response(array("success"=>true), 200);
            exit;
        }else{
            
            error_log("saving error [$reqid | kilimoOne | Tola]".date('Y-m-d H:i:s')."\n", 3, $logf);
            $this->response(array("success"=>false), 200);
            exit;
        }
        
    }
}