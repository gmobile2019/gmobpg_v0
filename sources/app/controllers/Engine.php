<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Engine
 *
 * @author gmobile
 */
class Engine extends CI_Controller{
    //put your code here
    
    function __construct() {
        parent::__construct();
        $this->load->model('Engine_model');
    }
    
    /*
     * Tola transactions processor
     */
    function kilimoOneProcessor($instance){
        $logs="./logs/kilimo_one_txns_".date('Y-m-d').".log";
        
        if(filesize($logs) >= 20000000){
            
            rename($logs, "./logs/kilimo_one_txns_".date('YmdHis')."_".date('Y-m-d').".log");
        }
        
        $lock =file_exists('./locks/kilimo_one_'.$instance.'.lock');
        if ($lock) {

            echo "LOCKED\n";
            exit;

        }else{
            
            $lk=fopen('./locks/kilimo_one_'.$instance.'.lock','w'); 
        }
        $counter=0;
        while($counter <= $this->config->item('pr_rounds')){
            $counter++;
            $unique=  uniqid();
            //retrieve tola unprocessed transactions
            $data=$this->Engine_model->kilimo_one_transactions(NULL,'pending',$this->config->item('kilimo_one_serviceID'));
            
            if($data == NULL){
                echo "no pending transactions [$unique] ".date('Y-m-d H:i:s')."\n";
                error_log("no pending transactions [$unique] ".date('Y-m-d H:i:s')."\n",3,$logs);
                sleep(10);
                continue;
            }
            
            $upd_sql=array();
            foreach($data AS $txn){
                
                $stt=$this->fetchKilimoOneOrder($txn->referenceNumber, $unique);
                
                if(!$stt){
                    continue;
                }
                
                
                if((string)$stt->orderID === NULL){
                    
                    echo "order id not found [$unique] ".date('Y-m-d H:i:s')."\n";
                    error_log("order id not found [$unique] ".date('Y-m-d H:i:s')."\n",3,$logs);
                    $upd_sql[]="UPDATE tola_transactions SET refundReason='Unknown_Reference_Number',status='processed',refundStatus='pending',lastupdate='".date('Y-m-d H:i:s')."' WHERE id='$txn->id'";
                    continue;
                }
                
                if((string)$stt->orderSTATUS <> 'confirmed'){
                    
                    echo "invalid order id [$unique] ".date('Y-m-d H:i:s')."\n";
                    error_log("invalid order id [$unique] ".date('Y-m-d H:i:s')."\n",3,$logs);
                    $upd_sql[]="UPDATE tola_transactions SET refundReason='Invalid_Reference_Number',status='processed',refundStatus='pending',lastupdate='".date('Y-m-d H:i:s')."' WHERE id='$txn->id'";
                    continue;
                }
                
                if((float)$stt->orderAMOUNT > $txn->amount){
                    
                    echo "amount not sufficient [$unique] ".date('Y-m-d H:i:s')."\n";
                    error_log("amount not sufficient [$unique] ".date('Y-m-d H:i:s')."\n",3,$logs);
                    $upd_sql[]="UPDATE tola_transactions SET refundReason='Insufficient_Amount',status='processed',refundStatus='pending',lastupdate='".date('Y-m-d H:i:s')."' WHERE id='$txn->id'";
                    continue;
                }
                
                $data= json_encode(array(
                                        'method'=>'postTransaction',
                                        'paymentMode'=>$txn->channel,
                                        'transactionID'=>$txn->operatorTxnID,
                                        'msisdn'=>$txn->msisdn,
                                        'orderID'=>$txn->referenceNumber,
                                        'transactionDate'=>$txn->receivedTimestamp, 	
                                        'amount'=>$txn->amount 	
                                        )
                                    );
                $post=$this->postKilimoOneTransaction($data,$unique);//post transaction
                
                if((string)$post->respSTATUS === 'success'){
                    
                    $upd_sql[]="UPDATE tola_transactions SET status='processed',lastupdate='".date('Y-m-d H:i:s')."' WHERE id='$txn->id'";
                }
            }
            
            if($upd_sql <> NULL){
                while(true){
                    
                    if($this->Engine_model->saveTransaction($upd_sql)){
                        break;
                    }
                }
            }
            
        }
        
        fclose($lk);
        unlink('./locks/kilimo_one_'.$instance.'.lock');
    }
    
    private function fetchKilimoOneOrder($orderID,$unique){
        $logs="./logs/kilimo_one_txns_".date('Y-m-d').".log";
        
        $data=json_encode(array('method'=>'getOrder','orderid'=>$orderID));
        
        echo "request : $data [$unique] ".date('Y-m-d H:i:s')."\n";
        error_log("request : $data [$unique] ".date('Y-m-d H:i:s')."\n",3,$logs);
        
        $crc=$this->config->item('authorization_code').' '.$data;
        $header= array('Content-Type: application/json','CRC: '.sha1($crc));

        $ch=curl_init($this->config->item('kilimo_one_endpoint'));

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_TIMEOUT,5);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response=curl_exec($ch);
        
        $c_errno=curl_errno($ch);
        $c_err=curl_error($ch);
       
        error_log("connection details : $c_errno ($c_err) [$unique] ".date('Y-m-d H:i:s')."\n", 3, $logs);
        echo "connection details : $c_errno ($c_err) [$unique] ".date('Y-m-d H:i:s')."\n";
        
        curl_close($ch);
        
        error_log("response : $response [$unique] ".date('Y-m-d H:i:s')."\n", 3, $logs);
        echo "response : $response [$unique] ".date('Y-m-d H:i:s')."\n";
        
        if($c_errno == 0){
            
            return json_decode($response);
        }
        
        return FALSE;
    }
    
    private function postKilimoOneTransaction($data,$unique){
        $logs="./logs/kilimo_one_txns_".date('Y-m-d').".log";
        
        echo "request : $data [$unique] ".date('Y-m-d H:i:s')."\n";
        error_log("request : $data [$unique] ".date('Y-m-d H:i:s')."\n",3,$logs);
        
        $crc=$this->config->item('authorization_code').' '.$data;
        $header= array('Content-Type: application/json','CRC: '.sha1($crc));

        $ch=curl_init($this->config->item('kilimo_one_endpoint'));

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_TIMEOUT,5);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response=curl_exec($ch);
        
        $c_errno=curl_errno($ch);
        $c_err=curl_error($ch);
       
        error_log("connection details : $c_errno ($c_err) [$unique] ".date('Y-m-d H:i:s')."\n", 3, $logs);
        echo "connection details : $c_errno ($c_err) [$unique] ".date('Y-m-d H:i:s')."\n";
        
        curl_close($ch);
        
        error_log("response : $response [$unique] ".date('Y-m-d H:i:s')."\n", 3, $logs);
        echo "response : $response [$unique] ".date('Y-m-d H:i:s')."\n";
        
        if($c_errno == 0){
            
            return json_decode($response);
        }
        
        return FALSE;
    }
}
